package com.example.spring.dao;

import com.example.spring.model.Student;

public interface StudentDAO {

	void save(Student thestudent);

	Student findById(Integer id);

	void update(Student students);

	void remove(Integer id);
}
