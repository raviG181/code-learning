package com.example.spring.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.spring.dao.StudentDAO;
import com.example.spring.model.Student;

import jakarta.persistence.EntityManager;

@Repository
public class StudnetDAOImpl implements StudentDAO {

	private EntityManager entityManager;

	@Autowired
	public StudnetDAOImpl(EntityManager theentityManager) {
		this.entityManager = theentityManager;
	}

	@Override
	@Transactional
	public void save(Student student) {
		entityManager.persist(student);
	}

	@Override
	public Student findById(Integer id) {
		return entityManager.find(Student.class, id);
	}

	@Override
	@Transactional
	public void update(Student students) {
		entityManager.merge(students);
	}

	@Override
	@Transactional
	public void remove(Integer id) {
		Student st = entityManager.find(Student.class, id);
		entityManager.remove(st);
	}
}
