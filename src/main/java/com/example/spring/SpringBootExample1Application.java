package com.example.spring;

import java.util.Scanner;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.example.spring.dao.StudentDAO;
import com.example.spring.model.Student;

@SpringBootApplication
public class SpringBootExample1Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootExample1Application.class, args);
	}

	@Bean
	public CommandLineRunner commandLineRunner(StudentDAO studentDAO) {
		Scanner s = new Scanner(System.in);
		System.out.println("Enter the task(create,read,update,delete): ");
		String task = s.next();
		return runner -> {
			switch (task.toLowerCase()) {

			case "create":
				createStudent(studentDAO);
				break;
			case "read":
				findBy(studentDAO);
				break;
			case "update":
				updateByID(studentDAO);
				break;
			case "delete":
				deleteByID(studentDAO);
				break;
			default:
				System.out.println("Invalid Selection");
			}
			s.close();
		};

	}

	private void createStudent(StudentDAO studentDAO) {
		Scanner s = new Scanner(System.in);
		String a = null, b = null;
		System.out.print("Enter no of student: ");
		int n = s.nextInt();
		Student[] st = new Student[n];
		for (int i = 0; i < n; i++) {
			System.out.println("Enter name: ");
			a = s.next();
			System.out.println("Enter city: ");
			b = s.next();
			System.out.println("Enter age: ");
			int age = s.nextInt();
			s.close();
			st[i] = new Student(a, b, age);
			studentDAO.save(st[i]);
		}
	}

	private void findBy(StudentDAO studentDAO) {
		Scanner s = new Scanner(System.in);
		System.out.println("Enter the id to fetch: ");
		int n = s.nextInt();
		s.close();
		Student st = studentDAO.findById(n);
		if (st == null) {
			System.out.println("Invalid ID !!!!!!!!!!!");
		} else
			System.out.println(st);
	}

	private void updateByID(StudentDAO studentDAO) {
		Scanner s = new Scanner(System.in);
		String name = null, city = null;
		int age = 0;
		System.out.print("Enter Id to update: ");
		int n = s.nextInt();
		Student st = studentDAO.findById(n);
		if (st != null) {
			System.out.print("Enter data to update : ");
			String value = s.next();
			if (value.equalsIgnoreCase("name")) {
				System.out.println("Enter name: ");
				name = s.next();
				st.setName(name);
				studentDAO.update(st);
			} else if (value.equalsIgnoreCase("city")) {
				System.out.println("Enter city: ");
				city = s.next();
				st.setCity(city);
				studentDAO.update(st);
			} else if (value.equalsIgnoreCase("age")) {
				System.out.println("Enter age: ");
				age = s.nextInt();
				st.setAge(age);
				studentDAO.update(st);
			}
			s.close();
		} else
			System.out.println("Inavlid ID");
	}

	private void deleteByID(StudentDAO studentDAO) {
		Scanner s = new Scanner(System.in);
		System.out.println("Enter student ID to remove: ");
		int n = s.nextInt();
		s.close();
		Student st = studentDAO.findById(n);
		if (st != null)
			studentDAO.remove(n);
		else
			System.out.println("Invalid ID");
	}
}
